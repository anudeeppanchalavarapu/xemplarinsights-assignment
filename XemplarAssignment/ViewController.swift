//
//  ViewController.swift
//  XemplarAssignment
//
//  Created by Anudeep Athreya on 10/06/20.
//  Copyright © 2020 Xemplar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
//
    @IBOutlet weak var outerCircleView: UIView!
    @IBOutlet weak var innerCircleViewOne: UIView!
    @IBOutlet weak var innerCircleViewTwo: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        outerCircleView.layer.cornerRadius = outerCircleView.frame.size.width / 2
        innerCircleViewOne.layer.cornerRadius = innerCircleViewOne.frame.size.width / 2
        innerCircleViewTwo.layer.cornerRadius = innerCircleViewTwo.frame.size.width / 2
    }
}

